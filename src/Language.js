import React from "react";
import './Language.less'
import Link from "./link/Link";

const Language = () => {
    return (
        <section>
            <span>
                Google offered in:
            </span>
            <span>
                <a href="https://www.google.com/setprefs?sig=0_rMHmKUJtsq3HBo0O7s4gV3Wf7s8%3D&hl=zh-TW&source=homepage&sa=X&ved=0ahUKEwiynp2WqOPkAhXSAYgKHeO9AxEQ2ZgBCA8">中文（繁體）</a>
            </span>
            <span>
                <a href="https://www.google.com/setprefs?sig=0_rMHmKUJtsq3HBo0O7s4gV3Wf7s8%3D&hl=zh-CN&source=homepage&sa=X&ved=0ahUKEwiynp2WqOPkAhXSAYgKHeO9AxEQ2ZgBCBA">中文(简体)</a>
            </span>
        </section>
    );
};
export default Language;
