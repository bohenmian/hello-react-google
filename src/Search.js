import React from 'react';
import './Search.less';
import {MdMic, MdSearch} from "react-icons/all";
import Button from "./button/Button";
import Language from "./Language";

const Search = () => {
    return (
        <section className='search'>
            <header>
                <img src="https://www.google.com/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png"
                     alt="Google Logo"/>
                <article>
                    <MdSearch className="search-icon"/>
                    <input type="text"/>
                    <MdMic className="voice-icon"/>
                </article>
                <section>
                    <Button value='Google Search'/>
                    <Button value='I m Feeling Lucky'/>
                </section>
                <Language/>
            </header>
        </section>
    );
};

export default Search;
