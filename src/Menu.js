import React from "react";
import './Menu.less';
import {MdApps} from 'react-icons/md';

const Menu = () => {
    return (
        <nav className='menu'>
            <ul>
                <li><a href="https://mail.google.com/mail/?tab=wm&ogbl">Gmail</a></li>
                <li><a href="https://www.google.com.hk/imghp?hl=en&tab=wi&ogbl">图片</a></li>
                <li><MdApps className='md-apps'/></li>
                <li className='profile'>S</li>
            </ul>
        </nav>
);
};

export default Menu;
