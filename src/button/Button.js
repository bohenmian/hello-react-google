import React from "react";
import './Button.less'

const Button = (props) => {
    return (
        <button>{props.value}</button>
    )
};
export default Button;
