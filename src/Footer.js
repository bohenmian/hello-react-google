import React from "react";
import './Footer.less'
import Link from "./link/Link";

const Footer = () => {
    const externalLink = ['Advertising', 'Business', 'About', 'How Search works'];
    const copyRightLink = ['Privacy', 'Terms', 'Settings'];
    return (
        <footer>
            <section>
                Hong Kong
            </section>
            <section>
                <Link value={externalLink}/>
                <Link value={copyRightLink}/>
            </section>
        </footer>
    );
};
export default Footer
